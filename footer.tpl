{if $loginpage eq 0 and $templatefile ne "clientregister"}
                        </div><!-- /.main-content -->
                <div class="col-md-3 pull-md-left sidebar">
                {if !$inShoppingCart && $secondarySidebar->hasChildren()}
                    <div>
                        {include file="$template/includes/sidebar.tpl" sidebar=$secondarySidebar}
                    </div>
                {/if}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
</div>
<div id="footer" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-5">
                <div class="footer-menu-holder">
                    <h4>DoopWeb</h4>
                    <p>Somos una agencia y plataforma de desarrollo web que hace las cosas bien. 
                        Optimizamos tu sitio con todos los elementos que Google exige para que se posicione de forma orgánica (velocidad, palabras clave, sin plantillas) </p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <div class="address-holder">
                    <div class="phone"><i class="fa fa-phone"></i> +52 (55) 7098 9673</div>
                    <div class="email"><i class="fa fa-envelope"></i> hola@doopweb.agency</div>
                    <div class="address">
                        <i class="fa fa-map-marker"></i> 
                        <div>Av. Chapultepec 554, Piso 2,<br>
                            Roma norte, 06700,<br>
                            Cuauhtémoc, CDMX.</div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="footer-menu-holder">
                    {if !$loggedin }
                    <h4>Links</h4>
                    {else}
                    <h4>Client details</h4>
                    {/if}
                    <ul class="footer-menu">
                        {if !$loggedin }
                            {include file="$template/includes/navbar.tpl" navbar=$primaryNavbar}
                        {else}
                            {include file="$template/includes/customnavbar.tpl" navbar=$secondaryNavbar}
                        {/if}
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-1 col-md-1">
                <div class="social-menu-holder">
                    <ul class="social-menu">
                        <li><a href="https://www.facebook.com/doopwebie" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/doopweb/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://twitter.com/DoopWeb" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal system-modal fade" id="modalAjax" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content panel panel-primary">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Cerrar</span>
                </button>
                <h4 class="modal-title">Title</h4>
            </div>
            <div class="modal-body panel-body">
                Loading...
            </div>
            <div class="modal-footer panel-footer">
                <div class="pull-left loader">
                    <i class="fa fa-circle-o-notch fa-spin"></i> Cargando...
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary modal-submit">
                    Submit
                </button>
            </div>
        </div>
    </div>
</div>
{/if}
{if $templatefile eq "clientregister"}
<script>
    $(window).on("load", function() {
        $("select").addClass("selectpicker");
    });
</script>
<script src="{$WEB_ROOT}/templates/{$template}/js/bootstrap-select.min.js"></script>
{/if}
<script src="{$WEB_ROOT}/templates/{$template}/js/bootstrap-slider.min.js"></script>
<script src="{$WEB_ROOT}/templates/{$template}/js/slick.min.js"></script>
<script src="{$WEB_ROOT}/templates/{$template}/js/main.js"></script>

{$footeroutput}

</body>
</html>
