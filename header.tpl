<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="{$charset}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{if $kbarticle.title}{$kbarticle.title} - {/if}{$companyname} - {$pagetitle}</title>

    {include file="$template/includes/head.tpl"}

    {$headoutput}

</head>
<body {if $loginpage eq 1 or $templatefile eq "clientregister"}class="fullpage"{/if}>
{if $loginpage eq 0 and $templatefile ne "clientregister"}

<div id="header-holder" class="{if $templatefile != 'homepage'}inner-header{/if}">
    <div class="bg-animation"></div>
    
    {$headeroutput}

    <section id="header" class="container-fluid">
        <div class="container">
            <ul class="top-nav">
                    <li>
                        <a href="https://doopweb.agency/blog/" target="_blank">Blog</a>
                    </li>
                    <!---
                    <li>
                        <a>Precios</a>
                    </li>
                    <li>
                        <a>Casos de éxito</a>
                    </li>
                    <li>
                        <a>|</a>
                    </li> ---->
                {if $languagechangeenabled && count($locales) > 1}
                    <li>
                        <a href="#" class="choose-language" data-toggle="popover" id="languageChooser">
                            {$activeLocale.localisedName}
                            <b class="caret"></b>
                        </a>
                    </li>
                {/if}
                {if $loggedin}
                    <li>
                        <a href="#" data-toggle="popover" id="accountNotifications" data-placement="bottom">
                            {$LANG.notifications}
                            {if count($clientAlerts) > 0}<span class="label label-info">Nuevo</span>{/if}
                            <b class="caret"></b>
                        </a>
                        <div id="accountNotificationsContent" class="hidden">
                            <ul class="client-alerts">
                            {foreach $clientAlerts as $alert}
                                <li>
                                    <a href="{$alert->getLink()}">
                                        <i class="fa fa-fw fa-{if $alert->getSeverity() == 'danger'}exclamation-circle{elseif $alert->getSeverity() == 'warning'}warning{elseif $alert->getSeverity() == 'info'}info-circle{else}check-circle{/if}"></i>
                                        <div class="message">{$alert->getMessage()}</div>
                                    </a>
                                </li>
                            {foreachelse}
                                <li class="none">
                                    {$LANG.notificationsnone}
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                    </li>
                    <li class="primary-action">
                        <a href="{$WEB_ROOT}/logout.php" class="btn btn-action">
                            {$LANG.clientareanavlogout}
                        </a>
                    </li>
                {else}
                    <li>
                        <a href="{$WEB_ROOT}/clientarea.php">{$LANG.login}</a>
                    </li>
                    {if $condlinks.allowClientRegistration}
                        <li>
                            <a href="{$WEB_ROOT}/register.php">{$LANG.register}</a>
                        </li>
                    {/if}
                    <li class="primary-action">
                        <a href="{$WEB_ROOT}/cart.php?a=view" class="btn btn-action">
                            {$LANG.viewcart}
                        </a>
                    </li>
                {/if}
                {if $adminMasqueradingAsClient || $adminLoggedIn}
                    <li>
                        <a href="{$WEB_ROOT}/logout.php?returntoadmin=1" class="btn btn-logged-in-admin" data-toggle="tooltip" data-placement="bottom" title="{if $adminMasqueradingAsClient}{$LANG.adminmasqueradingasclient} {$LANG.logoutandreturntoadminarea}{else}{$LANG.adminloggedin} {$LANG.returntoadminarea}{/if}">
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                {/if}
                <li class="support-button-holder support-dropdown"> 
                    <a class="support-button" href="#">Soporte</a>
                    <ul class="dropdown-menu">
                      <li><a href="#"><i class="fa fa-phone"></i>Llamanos: +525570989673</a></li>
                      <li><a href="#"><i class="fa fa-comments"></i>Iniciar un chat</a></li>
                      <li><a href="#"><i class="fa fa-ticket"></i>Abrir un ticket</a></li>
                      <li><a href="#"><i class="fa fa-book"></i>Base de conocimiento</a></li>
                    </ul>
                </li>
            </ul>
            <div id="languageChooserContent" class="hidden">
                    <ul>
                        {foreach $locales as $locale}
                            <li>
                                <a href="{$currentpagelinkback}language={$locale.language}">{$locale.localisedName}</a>
                            </li>
                        {/foreach}
                    </ul>
            </div>

            {if $assetLogoPath}
                <a href="{$WEB_ROOT}/index.php" class="logo"><img src="{$assetLogoPath}" alt="{$companyname}"></a>
            {else}
                <a href="{$WEB_ROOT}/index.php" class="logo logo-text logo-holder"><div class="logo" style="width:72px;height:28px" title="{$companyname}"></div></a>
            {/if}

        </div>
    </section>
    <!---
    <section id="main-menu">
        <nav id="nav" class="container-fluid navbar navbar-default navbar-main" role="navigation">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>


                <div class="collapse navbar-collapse" id="primary-nav">

                    <ul class="nav navbar-nav">

                        {include file="$template/includes/navbar.tpl" navbar=$primaryNavbar}

                    </ul>

                    <ul class="nav navbar-nav navbar-right">

                        {include file="$template/includes/navbar.tpl" navbar=$secondaryNavbar}
                        
                    </ul>

                </div>
            </div>
        </nav>
    </section>
    -->
    {if $templatefile == 'homepage'}
    <div id="top-content" class="container-fluid">
    <!-- <img src="{$WEB_ROOT}/templates/{$template}/images/doopweb-agency-logo.png" height="100" class="d-inline-block align-top" alt="DoopWeb Agency"> -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {if $registerdomainenabled || $transferdomainenabled}
                    <div class="big-title">Tu sitio web<br>pero con esteroides💪</div>
                    <div class="big-title">Adquiere tu propio sitio web desde $399 pesos al mes </div>
               <!-- <div class="domain-search-holder">
                        <form id="domain-search" method="post" action="domainchecker.php">
                            <input id="domain-text" type="text" name="domain" placeholder="{$LANG.exampledomain}" />
                            {if $registerdomainenabled}
                            <span class="inline-button">
                                <input id="search-btn" type="submit" name="submit" value="{$LANG.search}" />
                            </span>
                            {/if}
                            {if $transferdomainenabled}
                            <span class="inline-button">
                                <input id="transfer-btn" type="submit" name="transfer" value="{$LANG.domainstransfer}" />
                            </span>
                            {/if}
                        </form>
                        <div class="captcha-holder">{include file="$template/includes/captcha.tpl"}</div>
                    </div> -->
                    {else}
                        <div class="toparea-space"></div>
                    {/if}
                </div>
                <div class="col-md-12">
                    <div class="arrow-button-holder">
                        <a href="#custom-plan">
                            <div class="button-text">Ver nuestros planes</div>
                            <div class="arrow-icon">
                                <i class="htfy htfy-arrow-down"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {/if}
</div>
{if $templatefile == 'homepage'}
<div id="info" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="info-text">A partir del 2017, comenzamos a crear proyectos extraordinarios para nuestros clientes, creando su sitio web personalizado para su negocio a precios accesibles.</div>
                <!-- <a href="{$WEB_ROOT}/register.php" class="ybtn ybtn-purple ybtn-shadow">Crear tu cuenta</a> -->
            </div>
        </div>
    </div>
</div>
<div id="services" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row-title">Nuestros servicios</div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="service-box">
                    <div class="service-icon">
                        <img src="{$WEB_ROOT}/templates/{$template}/images/service-icon1.png" alt="">
                    </div>
                    <div class="service-title"><a href="#">Desarrollo Web</a></div>
                    <div class="service-details">
                        <p>No solo es un sitio web, es tu puerta a mostrar tu negocio para que más clientes te conozcan.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="service-box">
                    <div class="service-icon">
                        <img src="{$WEB_ROOT}/templates/{$template}/images/service-icon2.png" alt="">
                    </div>
                    <div class="service-title"><a href="#">Marketing Digital</a></div>
                    <div class="service-details">
                        <p>Te ayudamos a llegar a tus prospectos ideales donde esten buscando tu producto o servicio.</p>
                    </div>
                </div>
            </div>
            <!---
            <div class="col-sm-12 col-md-6">
                <div class="service-box">
                    <div class="service-icon">
                        <img src="{$WEB_ROOT}/templates/{$template}/images/service-icon3.png" alt="">
                    </div>
                    <div class="service-title"><a href="#">VPS Hosting</a></div>
                    <div class="service-details">
                        <p>At vero eos et accusamus et iusto odio dignissimos
ducimus qui blanditiis praesentium voluptatum div
atque corrupti quos dolores et quas molestias.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="service-box">
                    <div class="service-icon">
                        <img src="{$WEB_ROOT}/templates/{$template}/images/service-icon4.png" alt="">
                    </div>
                    <div class="service-title"><a href="#">Cloud Hosting</a></div>
                    <div class="service-details">
                        <p>At vero eos et accusamus et iusto odio dignissimos
ducimus qui blanditiis praesentium voluptatum div
atque corrupti quos dolores et quas molestias.</p>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<div id="message1" class="container-fluid message-area">
    <div class="bg-color"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="text-purple-light">¿Estás listo?</div>
                <div class="text-purple-dark">Crea tu cuenta o contáctanos.</div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="buttons-holder">
                    <a href="{$WEB_ROOT}/register.php" class="ybtn ybtn-purple">Crear tu cuenta</a><a href="{$WEB_ROOT}/contact.php" class="ybtn ybtn-white ybtn-shadow">Contáctanos</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="custom-plan" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4>Precios😉</h4>
                <p>Nuestros costos son muy accesibles, transparentes y sin engaños.</p>
            </div>
            <!-- Lista de precios -->
            <div id="pricing" class="container-fluid">
            <div class="bg-color"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row-title">Planes de desarrollo web</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="pricing-box pricing-color1 testimonials-slider">
                            <div class="pricing-content">
                                <div class="pricing-icon">
                                    <div class="special-gradiant"></div>
                                    <i class="htfy htfy-technology"></i>
                                </div>
                                <div class="pricing-title">Plan Light</div>
                                <div class="pricing-price">$399 MXN / Mes</div>
                                <div class="princing-price">"o"</div>
                                <div class="pricing-price">$3,990 MXN / Año</div>
                                <div class="pricing-price">Cero Plantillas</div>
                                <div class="pricing-details">
                                    <ul>
                                        <li>1 apartado o sección</li>
                                        <li>1 modificación al mes</li>
                                        <li>Visibilidad con 5 palabras clave para Google y Bing</li>
                                        <li>Tu sitio listo en tan sólo 4 días hábiles</li>
                                        <li>Soporte Técnico prioritario</li>
                                    </ul>
                                </div>
                                <div class="pricing-link">
                                    <a class="ybtn" href="https://doopweb.agency/cart.php?a=add&pid=1">Crea tu sitio web</a>
                                    <div></div>
                                    <!-- Click to call STARTS HERE -->
                                    <script id="c2c-button" data-responsive="responsive" src="//apps.netelip.com/clicktocall/api2/js/api2.js?btnid=2746&atk=e95528d321dee81cc7c9b6b1c65bf3a7" type="text/javascript"></script> 
                                    <!-- Click to call ENDS HERE -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="pricing-box pricing-color2 featured">
                            <div class="pricing-content">
                                <div class="pricing-icon">
                                    <div class="special-gradiant"></div>
                                    <i class="htfy htfy-cloud"></i>
                                </div>
                                <div class="pricing-title">Plan extendido</div>
                                <div class="pricing-price">$699 MXN / Mes</div>
                                <div class="pricing-price">"o"</div>
                                <div class="pricing-price">$6,990 MXN / Año</div>
                                <div class="pricing-price">Cero Plantillas</div>
                                <div class="pricing-details">
                                    <ul>
                                        <li>Hasta 4 apartados o secciones</li>
                                        <li>+ 2 modificaciones extra al mes</li>
                                        <li>+ 5 palabras clave extra para Google y Bing</li>
                                        <li>Tu sitio listo en tan sólo 7 días hábiles</li>
                                        <li>Soporte técnico prioritario</li>
                                    </ul>
                                </div>
                                <div class="pricing-link">
                                    <a class="ybtn" href="https://doopweb.agency/cart.php?a=add&pid=2">Crea tu sitio web</a>
                                    <div></div>
                                    <!-- Click to call STARTS HERE -->
                                    <script id="c2c-button" data-responsive="responsive" src="//apps.netelip.com/clicktocall/api2/js/api2.js?btnid=2746&atk=e95528d321dee81cc7c9b6b1c65bf3a7" type="text/javascript"></script> 
                                    <!-- Click to call ENDS HERE -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="pricing-box pricing-color3">
                            <div class="pricing-content">
                                <div class="pricing-icon">
                                    <div class="special-gradiant"></div>
                                    <img src="{$WEB_ROOT}/templates/{$template}/images/pricing1.svg" alt="">
                                </div>
                                <div class="pricing-title">Plan tienda web</div>
                                <div class="pricing-price">$1199 MXN / Mes</div>
                                <div class="pricing-price">"o"</div>
                                <div class="pricing-price">$11,990 MXN / Año</div>
                                <div class="pricing-price">Cero Plantillas</div>
                                <div class="pricing-details">
                                    <ul>
                                        <li>100% autoadministrable</li>
                                        <li>Te añadimos hasta 15 artículos</li>
                                        <li>Integración con medios de pago y formas de envío</li>
                                        <li>Chat en linea</li>
                                        <li>Capacitación para usar la tienda</li>
                                    </ul>
                                </div>
                                <div class="pricing-link">
                                    <a class="ybtn" href="https://doopweb.agency/cart.php?a=add&pid=3">Crea tu sitio web</a>
                                    <div></div>
                                    <!-- Click to call STARTS HERE -->
                                    <script id="c2c-button" data-responsive="responsive" src="//apps.netelip.com/clicktocall/api2/js/api2.js?btnid=2746&atk=e95528d321dee81cc7c9b6b1c65bf3a7" type="text/javascript"></script> 
                                    <!-- Click to call ENDS HERE -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<div id="ifeatures" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row-title">¿Qué nos hace especiales?</div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="feature-box">
                    <div class="feature-icon">
                        <img src="{$WEB_ROOT}/templates/{$template}/images/feature1.svg" alt="">
                    </div>
                    <div class="feature-title">Infraestructura solida</div>
                    <div class="feature-details">Contamos con servidores e infraestructura robusta para que tu sitio este 100% activo siempre que lo necesites. </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="feature-box">
                    <div class="feature-icon">
                        <img src="{$WEB_ROOT}/templates/{$template}/images/feature2.svg" alt="">
                    </div>
                    <div class="feature-title">Actualiza y escala sin límites</div>
                    <div class="feature-details">¿Cansado de buscar programador? Puedes modificar tu sitio web sin costo adicional. </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="feature-box">
                    <div class="feature-icon">
                        <img src="{$WEB_ROOT}/templates/{$template}/images/feature3.svg" alt="">
                    </div>
                    <div class="feature-title">Seguridad primero</div>
                    <div class="feature-details">Contamos con protección extra y certificados SSL para que tu sitio este protegido siempre.</div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="feature-box">
                    <div class="feature-icon">
                        <img src="{$WEB_ROOT}/templates/{$template}/images/feature4.svg" alt="">
                    </div>
                    <div class="feature-title">100% soporte al cliente</div>
                    <div class="feature-details">Mantén tu sitio siempre actualizado para lograr el éxito. Contamos con soporte prioritario a nuestros clientes. </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="features" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row-title">Todas las características</div>
            </div>
        </div>
        <div class="row rtl-cols">
            <div class="col-sm-12 col-md-6">
                <div id="features-links-holder">
                    <div class="icons-axis">
                        <img src="{$WEB_ROOT}/templates/{$template}/images/browser.svg" alt="">
                    </div>
                    <div class="feature-icon-holder feature-icon-holder1 opened" data-id="1">
                        <div class="animation-holder"><div class="special-gradiant"></div></div>
                        <div class="feature-icon"><i class="htfy htfy-worldwide"></i></div>
                        <div class="feature-title">+ adaptable</div>
                    </div>
                    <div class="feature-icon-holder feature-icon-holder2" data-id="2">
                        <div class="animation-holder"><div class="special-gradiant"></div></div>
                        <div class="feature-icon"><i class="htfy htfy-cogwheel"></i></div>
                        <div class="feature-title">+ seguro</div>
                    </div>
                    <div class="feature-icon-holder feature-icon-holder3" data-id="3">
                        <div class="animation-holder"><div class="special-gradiant"></div></div>
                        <div class="feature-icon"><i class="htfy htfy-location"></i></div>
                        <div class="feature-title">+ rápido</div>
                    </div>
                    <div class="feature-icon-holder feature-icon-holder4" data-id="4">
                        <div class="animation-holder"><div class="special-gradiant"></div></div>
                        <div class="feature-icon"><i class="htfy htfy-download"></i></div>
                        <div class="feature-title">+ económico</div>
                    </div>
                    <div class="feature-icon-holder feature-icon-holder5" data-id="5">
                        <div class="animation-holder"><div class="special-gradiant"></div></div>
                        <div class="feature-icon"><i class="htfy htfy-like"></i></div>
                        <div class="feature-title">Soporte 24/7</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div id="features-holder">
                    <div class="feature-box feature-d1 show-details">
                        <div class="feature-title-holder">
                            <span class="feature-icon"><i class="htfy htfy-worldwide"></i></span>
                            <span class="feature-title">+ adaptable</span>
                        </div>
                        <div class="feature-details">
                            <p>Tu sitio será compatible con la mayoría de los teléfonos móviles.</p>

                            <p></p>
                        </div>
                    </div>
                    <div class="feature-box feature-d2">
                        <div class="feature-title-holder">
                            <span class="feature-icon"><i class="htfy htfy-cogwheel"></i></span>
                            <span class="feature-title">+ seguro</span>
                        </div>
                        <div class="feature-details">
                            <p>Seguridad en tu proyecto, con certificado SSL
                                 y encriptación de información entre tu sitio web y tu cliente.</p>

                            <p></p>
                        </div>
                    </div>
                    <div class="feature-box feature-d3">
                        <div class="feature-title-holder">
                            <span class="feature-icon"><i class="htfy htfy-location"></i></span>
                            <span class="feature-title">+ rápido</span>
                        </div>
                        <div class="feature-details">
                            <p>Gracias al CDN que integramos, lograrás la carga más rapida 
                                y completa de tu sitio web, así no perderás clientes.
                            </p>

                            <p></p>
                        </div>
                    </div>
                    <div class="feature-box feature-d4">
                        <div class="feature-title-holder">
                            <span class="feature-icon"><i class="htfy htfy-download"></i></span>
                            <span class="feature-title">+ económico</span>
                        </div>
                        <div class="feature-details">
                            <p>No te preocupes, nuestros precios son muy económicos para poder lanzar
                                tu idea a internet.
                            </p>

                            <p></p>
                        </div>
                    </div>
                    <div class="feature-box feature-d5">
                        <div class="feature-title-holder">
                            <span class="feature-icon"><i class="htfy htfy-like"></i></span>
                            <span class="feature-title">Soporte 24/7</span>
                        </div>
                        <div class="feature-details">
                            <p>¿Tienes dudas? Puedes contactarnos en cualquiera de los medios disponibles para responder
                                todas tus preguntas.
                            </p>

                            <p></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div id="testimonials" class="container-fluid">
    <div class="bg-color"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row-title">Nuestro clientes opinan de nuestro servicio</div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="testimonials-slider">
                    <div>
                        <div class="details-holder">
                            <img class="photo" src="{$WEB_ROOT}/templates/{$template}/images/person1.jpg" alt="">
                            <h4>Víctor Gutiérrez</h4>
                            <h5>Director General de Evolución TV</h5>
                            <p>Es un agrado contar con la experiencia y el servicio para hospedar y gestionar mi proyecto.</p>
                        </div>
                    </div>
                    <div>
                        <div class="details-holder">
                            <img class="photo" src="{$WEB_ROOT}/templates/{$template}/images/person2.jpg" alt="">
                            <h4>Monserrat Guerrero</h4>
                            <h5>Directora General de Obsession Nails</h5>
                            <p>Gracias a DoopWeb hemos tenido respuesta rapida para los cambios importantes de mi sitio web.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="more-features" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row-title">Somos... 😏</div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="mfeature-box">
                    <div class="mfeature-icon">

                        <img src="{$WEB_ROOT}/templates/{$template}/images/idea.svg" width="100" height="100" alt="">
                    </div>
                    <div class="mfeature-title">Creativos</div>
                    <div class="mfeature-details">Transformamos tus ideas en proyectos y los proyectos en beneficios.</div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="mfeature-box">
                    <div class="mfeature-icon">

                        <img src="{$WEB_ROOT}/templates/{$template}/images/like.svg" width="100" height="100" alt="">
                    </div>
                    <div class="mfeature-title">Únicos</div>
                    <div class="mfeature-details">Usamos código puro en vez de plantillas pero con precios accesibles.</div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="mfeature-box">
                    <div class="mfeature-icon">

                        <img src="{$WEB_ROOT}/templates/{$template}/images/laptop.svg" width="100" height="100" alt="">
                    </div>
                    <div class="mfeature-title">Efectivos</div>
                    <div class="mfeature-details">Creamos estrategias adaptables y funcionales basadas en tecnología y diseño.</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="message2" class="container-fluid message-area normal-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="text-purple-light">¿Convencido?</div>
                <div class="text-purple-dark">¡Crea tu cuenta ahora!</div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="buttons-holder">
                    <a href="{$WEB_ROOT}/register.php" class="ybtn ybtn-purple">Crear tu cuenta</a><a href="{$WEB_ROOT}/contact.php" class="ybtn ybtn-white ybtn-shadow">Contáctanos hoy mismo</a>
                </div>
            </div>
        </div>
    </div>
</div>
{/if}

{include file="$template/includes/verifyemail.tpl"}
<div id="main-body-holder" class="container-fluid">
<section id="main-body">
    <div class="container{if $skipMainBodyContainer}-fluid without-padding{/if}">
        <div class="row">

        {if !$inShoppingCart && ($primarySidebar->hasChildren() || $secondarySidebar->hasChildren())}
            {if $primarySidebar->hasChildren() && !$skipMainBodyContainer}
                <div class="col-md-9 pull-md-right">
                    {include file="$template/includes/pageheader.tpl" title=$displayTitle desc=$tagline showbreadcrumb=true}
                </div>
            {/if}
            <div class="col-md-3 pull-md-left sidebar">
                {include file="$template/includes/sidebar.tpl" sidebar=$primarySidebar}
            </div>
        {/if}
        <!-- Container for main page display content -->
        <div class="{if !$inShoppingCart && ($primarySidebar->hasChildren() || $secondarySidebar->hasChildren())}col-md-9 pull-md-right{else}col-xs-12{/if} main-content">
            {if !$primarySidebar->hasChildren() && !$showingLoginPage && !$inShoppingCart && $templatefile != 'homepage' && !$skipMainBodyContainer}
                {include file="$template/includes/pageheader.tpl" title=$displayTitle desc=$tagline showbreadcrumb=true}
            {/if}
{/if}